# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 10:22:36 2016

@author: demerzel
"""
# python 2
#
# Name: Karthik Mohan
#
import random
class Board:
    """ a datatype representing a C4 board
        with an arbitrary number of rows and cols
    """
    
    def __init__( self, width, height ):
        """ the constructor for objects of type Board """
        self.width = width
        self.height = height
        W = self.width
        H = self.height
        self.data = [ [' ']*W for row in range(H) ]

        # we do not need to return inside a constructor!
        

    def __repr__(self):
        """ this method returns a string representation
            for an object of type Board
        """
        H = self.height
        W = self.width
        s = ''   # the string to return
        for row in range(0,H):
            s += '|'   
            for col in range(0,W):
                s += self.data[row][col] + '|'
            s += '\n'

        s += (2*W+1) * '-'    # bottom of the board
        
        # and the numbers underneath here
        s += '\n'
        for i in range(0,W):
            s += ' ' + str(i % 10)
        return s       # the board is complete, return it
    
    def addMove(self, col, ox):
        """ This method has 2 inputs. col represents the column in which the character is to be added and ox represents the character - either X or O """
        for i in range(self.height-1,-1,-1):
            if self.data[i][col] == ' ':
                self.data[i][col] = ox
                break
            else:
                continue
    
    def clear( self ):
        """ Clears the board """
        for i in range(self.height):
            for j in range(self.width):
                self.data[i][j] = ' '
        
    def setBoard( self, moveString ):
        """ takes in a string of columns and places
            alternating checkers in those columns,
            starting with 'X'
            
            For example, call b.setBoard('012345')
            to see 'X's and 'O's alternate on the
            bottom row, or b.setBoard('000000') to
            see them alternate in the left column.

            moveString must be a string of integers
        """
        nextCh = 'X'   # start by playing 'X'
        for colString in moveString:
            col = int(colString)
            if 0 <= col <= self.width:
                self.addMove(col, nextCh)
            if nextCh == 'X': nextCh = 'O'
            else: nextCh = 'X'
            
    def allowsMove(self, c):
        """ Method to detect the validity of a move """
        if c + 1 > self.width or c < 0:
            return False
        elif all(self.data[i][c] != ' ' for i in range(self.height -1,-1,-1)):
            return False
        return True
        
    def isFull(self):
        """ Method to detect if the board is full """
        if any(self.allowsMove(c) == True for c in range(self.width)):
            return False
        return True
    
    def delMove(self, c):
        """ Removes the top most checker from column c """
        for i in range(self.height):
            if self.data[i][c] == ' ':
                continue
            else:
                self.data[i][c] = ' '
                break
    
    def winsFor(self, ox):
        """ Determines if someone is the winner """
        H = self.height
        W = self.width
        D = self.data
        # check for horizontal wins
        for row in range(0,H):
            for col in range(0,W-3):
                if D[row][col] == ox and \
                   D[row][col+1] == ox and \
                   D[row][col+2] == ox and \
                   D[row][col+3] == ox:
                    return True
        # check for vertical wins
        for row in range(H-1,2,-1):
            for col in range(0,W):
                if D[row][col] == ox and \
                   D[row-1][col] == ox and \
                   D[row-2][col] == ox and \
                   D[row-3][col] == ox:
                    return True
        # check for northeast wins
        for row in range(H-1,2,-1):
            for col in range(0,W-3):
                if D[row][col] == ox and \
                   D[row-1][col+1] == ox and \
                   D[row-2][col+2] == ox and \
                   D[row-3][col+3] == ox:
                    return True
        # check for southeast wins
        for row in range(0,H-3):
            for col in range(0,W-3):
                if D[row][col] == ox and \
                   D[row+1][col+1] == ox and \
                   D[row+2][col+2] == ox and \
                   D[row+3][col+3] == ox:
                    return True
        return False
        
    
    def hostGame(self):
        """ Lets play a game """
        print "Welcome to Connect Four!"
        print self
        # while loop
        while True:
            x = -1
            while self.allowsMove(x) == False:   # check for validity
                x = input("X's turn. Choose a column between 0 and 6: ") # ask for x's choice
            self.addMove(x,'X')  # input it into the board
            print self  # print board
            if self.winsFor('X'): # check if X won
                print "\n X wins! "
                break
            o = -1 
            while self.allowsMove(o) == False:   # check for validity
                o = input("O's turn. Choose a column between 0 and 6: ")   # ask for o's choice
            self.addMove(o,'O')        # input into board
            print self
            if self.winsFor('O'):   # check if O won
                print "\n O wins! "
                break
            elif self.isFull():
                    print " Tie Game!"
                    break

    def playGame(self,px,po):
        print "Welcome to Connect Four!"
        print self
        # while loop
        while True:
            if px == 'human' :
                x = -1
                while self.allowsMove(x) == False:   # check for validity
                    x = input("X's turn. Choose a column between 0 and 6: ") # ask for x's choice
                self.addMove(x,'X')  # input it into the board
            else:
                x = px.nextMove(self)
                self.addMove(x,'X')
            print self  # print board
            if self.winsFor('X'): # check if X won
                print "\n X wins! "
                break
            if po == 'human':
                o = -1 
                while self.allowsMove(o) == False:   # check for validity
                    o = input("O's turn. Choose a column between 0 and 6: ")   # ask for o's choice
                self.addMove(o,'O')        # input into board
            else:
                o = po.nextMove(self)
                self.addMove(o,'O')
            print self
            if self.winsFor('O'):   # check if O won
                print "\n O wins! "
                break
            elif self.isFull():
                    print " Tie Game!"
                    break



class Player:
    """ A data type representing a player AI that plays C4 """
    def __init__(self, ox, tbt, ply):
        """ the constructor for objects of type Player """
        self.ox = ox
        self.tbt = tbt
        self.ply = ply
    
    def __repr__(self):
        """ This is a method that returns a string representing the Player object that calls it """
        s = "Player for " + self.ox + "\n"
        s += "  with tiebreak type: " + self.tbt + "\n"
        s += "  and ply == " + str(self.ply) + "\n\n"
        return s
    
    def oppCh(self):
        """ This method returns the opposing player's character """
        if self.ox == 'X':
            return 'O'
        else: return 'X'
    
    def scoreBoard(self, b):
        """ Method returns the value of the current board according to the scale mentioned, i.e : 100 if winning, 15 if neutral, 0.0 if its a loss and -1 if the board is full """
        if b.winsFor(self.ox) == True:
            return 100.0
        elif b.winsFor(self.oppCh()) == True:
            return 0.0
        else:
            return 50.0
        
    def tiebreakMove(self, scores):
        """ This method returns column number (index) of the highest score from a score list, taking into account the tie breaking method employed """
        mais = ()
        for i in range(len(scores)):
            if scores[i] == max(scores):
                mais += (i,)
        if len(mais) != 1:
            if self.tbt == 'LEFT':
                return mais[0]
            elif self.tbt == 'RIGHT':
                return mais[-1]
            elif self.tbt == 'RANDOM':
                return random.choice(mais)
        else:
            return mais[0]
    
    def scoresFor(self, b):
        """ Method to return a list of scores with the 'c'th score representing the goodness of the input after player moves to column c """
        scores = [50]*b.width
        for i in range(b.width):
            if all(b.data[c][i] != ' ' for c in range(b.height)):
                scores[i] = -1.0
            elif self.scoreBoard(b) != 50.0 :
                scores[i] = self.scoreBoard(b)
                break
            elif self.ply == 0:
                continue
            else:
                b.addMove(i,self.ox)
                if self.scoreBoard(b) == 100.0 :
                    scores[i] = self.scoreBoard(b)
                    b.delMove(i)
                else:
                    p2list = Player(self.oppCh(), self.tbt, (self.ply - 1) ).scoresFor(b)
                    if any(d == 100.0 for d in p2list) :
                        scores[i] = 0.0
                    elif all(d == 0.0 for d in p2list) :
                        scores[i] = 100.0
                    b.delMove(i)
        return scores
    
    def nextMove(self,b):
        """ Method takes into account the data from scoresFor and Tiebreakmove and returns the column number to be moved into """
        return self.tiebreakMove(self.scoresFor(b))

b = Board(7,6)
px = 'human'
po = Player('O','LEFT',3)
b.playGame(px,po)
# README #
Connect4 A.I.

* I created this code to help play the connect4 game against computer A.I. The A.I. is fairly basic. This was created just for fun.
* Version : This is the final version with my know-how, which works out of the box. If I come across a better method to address the AI, I shall do so in the future.


### How do I get set up? ###

* Code works out of the box. Just hit run and play it in the console
* Python 2.7
* Requires random module of python 2.7
* The last 4 lines of the code will help modify it according to how you want to play
* Set the board length to different parameters in line 283 if you don't want a width of 7 and a height of 6
* Set px or po as 'human' (string) in order to play against the A.I. px if you want to play as 'X' and po if you want to play as 'O'
* In the AI section, say po = Player('O','LEFT',2), the 2nd argument ('LEFT') denotes the direction which the AI is going to focus on. This doesn't change the fact that AI will consider your moves and act accordingly. The 3 options are 'LEFT', 'RIGHT' and 'RANDOM'. The number 2, which is the last argument, denotes the number of moves the AI has to think ahead. The higher the number, the longer it takes the AI to simulate.

### Who do I talk to? ###

* **Karthik Mohan**
* *email : karthik.mohan390@gmail.com*